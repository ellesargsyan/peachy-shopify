import $ from 'jquery';
import Swiper, { Pagination } from 'swiper';
import LazyLoad from "vanilla-lazyload";

Swiper.use([Pagination]);

export default class Slider {
     initMoreRecipeCarousel() {
        if ($(window).width() <= 991.8) {
            new Swiper('.product--slider', {
                slidesPerView: 1,
                spaceBetween: 10,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                },
                breakpoints: {
                    768: {
                        slidesPerView: 2
                    }
                },
                on: {
                    afterInit: (swiper) => {
                        new LazyLoad({
                            container: swiper.el,
                            cancel_on_exit: false
                        })
                    }
                }
            })
        }
    }
}