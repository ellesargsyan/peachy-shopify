import $ from 'jquery'
import LazyLoad from 'vanilla-lazyload'

$(() => {
    const $body = $('body')
    $body.on('click', '.close-bag', function () {
        $('.sidebar-wrapper').removeClass('open')
    })
    $body.on('submit', '.AddToCartForm', function (e) {
        e.preventDefault()
        const $form = $(this)
        console.log($form.serializeArray())
        $.ajax({
            url: '/cart/add.js',
            dataType: 'json',
            cache: false,
            type: 'post',
            data: $form.serialize(),
            success: function () {
                updateCart(false)
                $('.sidebar-wrapper').addClass('open')
            }
        })
    })

    $body.on('submit', '.ChangeCartForm', function (e) {
        e.preventDefault()
        const $form = $(this)
        $.ajax({
            url: '/cart/change.js',
            dataType: 'json',
            cache: false,
            type: 'post',
            data: $form.serialize(),
            success: function (res) {
                updateCart(!res.items.length)
            }
        })
    })

    $body.on('change', '.variant-item', function  (e) {
        const $this = $(this);
        console.log($this)
        const $vartiantValue = $this.val();
        $(this).parents('.card__sizes').find('.productVariant').val($vartiantValue);
    });

    $body.on('change', '.cart-ctm-select', function (e) {
        e.preventDefault();
        const $this = $(this);
        const id = $('.cart-item__select').parent().data('variant_id');
        console.log($this, id )
        $.ajax({
            url: '/cart/change.js',
            dataType: 'json',
            cache: false,
            type: 'post',
            data: {
                id,
                quantity: 0
            },
            success: function () {
                $.ajax({
                    url: '/cart/add.js',
                    dataType: 'json',
                    cache: false,
                    type: 'post',
                    data: $this.serialize(),
                    success: function () {
                        updateCart(false);
                    }
                });
            }
        });

    });

    const updateCart = () => {
        return $.ajax({
            url: `/cart?view=header`,
            success: (response) => {
                $('.bag-wrapper').html($(response).html())
                new LazyLoad()
            },
        });
    };
})
