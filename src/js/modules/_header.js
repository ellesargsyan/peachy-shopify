import $ from 'jquery';
import LazyLoad from 'vanilla-lazyload';
import { throttle } from './_utils';

export default class Header {
	constructor() {
		this.header = $('.header');
		this.nav = $('.nav');
		this.navList = $('.nav__list');
		this.navbarTogglerBtn = $('.navbar-toggler');
		this.promoBanner = $('.promo-banner');
		this.init();
	}

	init() {
		this.toggleNav();
		this.isNotificationOpen();
		this.collpaseDropdown();
		this.stickyHeader();
		this.loadLazyImages();
	}

	toggleNav() {
		let _this = this;
		$(this.navbarTogglerBtn).on('click', function () {
			$(this).toggleClass('navbar-toggler--open');
			$(_this.nav).fadeToggle(0);
			$('body').toggleClass('nav-open');
		});
	}

	isNotificationOpen() {
		if ($(this.promoBanner).is(':visible')) {
			$('body').addClass('notification-open');
		}
	}

	collpaseDropdown() {
		$(this.navList).on('click', '.navbar__dropdown-btn', function () {
			let parent = $(this).parent();
			parent.siblings('.nav__item').find('.navbar__dropdown-btn').removeClass('navbar__dropdown-btn--active');
			$(this).toggleClass('navbar__dropdown-btn--active');
			parent.siblings('.nav__item').find('.dropdown').slideUp(500);
			parent.find('.dropdown').slideToggle(500);
		});
	}

	stickyHeader() {
		const headerEl = this.header,
			headerHeight = headerEl.outerHeight(),
			promoBannerHeight = this.promoBanner.outerHeight(),
			headerPosition = promoBannerHeight + headerHeight + 100,
			collectionBanner = $('.collection-top').outerHeight(),
			collectionBannerPosition = promoBannerHeight +  collectionBanner;

		let lastScrollTop = 0;
		$(window).on('scroll', throttle(setStickyHeader, 10));

		function setStickyHeader() {
			let scrollPosition = $(window).scrollTop();

			if (scrollPosition >= headerPosition ) {
				headerEl.addClass('header--sticky');
			} else {
				headerEl.removeClass('header--sticky');
			}

			if (scrollPosition > lastScrollTop) {
				$('body').removeClass('scroll-up').addClass('scroll-down');
			} else {
				$('body').addClass('scroll-up').removeClass('scroll-down');
			}

			if (scrollPosition >= collectionBannerPosition) {
				$('.product-checkbox__list-wrap').fadeIn();
			} else {
				$('.product-checkbox__list-wrap').fadeOut();
			}

			lastScrollTop = scrollPosition;
		}
	}

	loadLazyImages() {
		$('.nav__link, .navbar__dropdown-btn, .navbar-toggler').on('mouseenter click', mouseoverHandler);

		function mouseoverHandler(event) {
			var navItem = event.currentTarget.parentElement;
			var lazyImg = navItem.querySelectorAll('.dropdown-lazy');
			lazyImg.forEach((element) => {
				if (!!element.getAttribute('data-ll-status')) {
					return;
				}
				LazyLoad.load(element);
			});
		}
	}
}
