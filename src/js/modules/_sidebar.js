import $ from "jquery";

export default class Sidebar {
    constructor() {
        this.init()
    }

    init() {
        this.toggleSidebar()
        this.bagSidebar()
    }

    //Sidebar
    toggleSidebar() {
        if ($(window).width() < 992) {
            $('.sidebar-toggle').click(function (e) {
                $('.sidebar-wrapper').css('position', 'relative')
                $('.bag-wrapper').addClass('d-none')
                $('.navbar-toggler').removeClass('navbar-toggler--open')
                $('.nav').fadeOut(200)
                e.preventDefault()
            })
            $('.close-sidebar').click(function () {
                setTimeout(() => {
                    $('.sidebar-wrapper').css('position', '')
                    $('.bag-wrapper').removeClass('d-none')
                    $('.login-form').removeClass('d-none')
                    $('.password-reset').addClass('d-none')
                    $('.signup-form').addClass('d-none')
                }, 500)
                $(document.body).removeClass('nav-open')
                $('.sidebar-wrapper').removeClass('login-open')
            })
        } else {
            $('.sidebar-toggle').click(function (e) {
                e.preventDefault()
                $('.sidebar-wrapper').toggleClass('open')
                $('.login-sidebar').removeClass('d-none')
                $('.bag-wrapper').addClass('d-none')
            })
            $('.close-sidebar').click(function () {
                $('.sidebar-wrapper').toggleClass('open')
                $('.bag-wrapper').removeClass('d-none')
                setTimeout(() => {
                    $('.login-form').removeClass('d-none')
                    $('.password-reset').addClass('d-none')
                    $('.signup-form').addClass('d-none')
                }, 300)
            })
        }

        $('.background').click(function () {
            $('.sidebar-wrapper').removeClass('open')
            $('.login-sidebar').removeClass('d-none')
            $('.bag-wrapper').removeClass('d-none')
            setTimeout(() => {
                $('.login-form').removeClass('d-none')
                $('.password-reset').addClass('d-none')
                $('.signup-form').addClass('d-none')
            }, 300)
        })
    }

    // Bag sidebar
    bagSidebar() {
        $('.cart-btn').click(function (e) {
            $('.sidebar-wrapper').toggleClass('open')
            $('.login-sidebar').addClass('d-none')
            $('.bag-wrapper').removeClass('d-none')
            e.preventDefault()
        })

        $('.close-bag').click(function () {
            $('.sidebar-wrapper').toggleClass('open')
            $('.login-sidebar').removeClass('d-none')
        })
    }
}
