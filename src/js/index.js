import $ from "jquery";
import LazyLoad from 'vanilla-lazyload';
import selectric from 'selectric';

//modules
import Header from './modules/_header';
import Sidebar from './modules/_sidebar';
import Slider from './modules/_swiper';
import './modules/_cart';

$(document).ready(function () {
    let lazy = new LazyLoad();
    // let global = new Global();

    //header
    let header = new Header();

    //Sidebar
    let sidebar = new Sidebar();

    new Slider().initMoreRecipeCarousel()

    // Selectric
    $('.selectric').selectric({
        disableOnMobile: false,
        nativeOnMobile: false,
    });

    $('body').on('click', '.atc-quantity button', function () {
        const $input = $(this).parent().find('input');
        const value = parseInt($input.val());
        if ($(this).hasClass('atc-quantity__minus')) {
            if (value === 1) {
                $input.val(1);
                return;
            }
            $input.val(value - 1);
        } else {
            $input.val(value + 1);
        }
    });
});