'use strict';

const {src, dest} = require('gulp');
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cssbeautify = require('gulp-cssbeautify');
const removeComments = require('gulp-strip-css-comments');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const notify = require('gulp-notify');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');

/* Paths */
const srcPath = 'src/';
const distPath = 'shopify/assets/';

const config = {
	shape: {
		spacing: {
			padding: 1,
		},
	},
	mode: {
		view: {
			// Activate the «view» mode
			bust: false,
			render: {
				scss: true, // Activate Sass output (with default options)
			},
		},
		symbol: true, // Activate the «symbol» mode
	},
};

const path = {
	build: {
		js: distPath + '/',
		css: distPath + '/',
		fonts: distPath + '/',
	},
	src: {
		js: srcPath + 'js/*.js',
		css: srcPath + 'scss/*.scss',
		fonts: srcPath + 'fonts/**/*.{eot,woff,woff2,ttf,svg}',
	},
	watch: {
		js: srcPath + 'js/**/*.js',
		css: srcPath + 'scss/**/*.scss',
		fonts: srcPath + 'fonts/**/*.{eot,woff,woff2,ttf,svg}',
	},
	clean: './' + distPath,
};

/* Tasks */


function css(cb) {
	return src(path.src.css, {base: srcPath + 'assets/scss/'})
		.pipe(
			plumber({
				errorHandler: function (err) {
					notify.onError({
						title: 'SCSS Error',
						message: 'Error: <%= error.message %>',
					})(err);
					this.emit('end');
				},
			})
		)
		.pipe(
			sass({
				includePaths: './node_modules/',
			})
		)
		.pipe(
			autoprefixer({
				cascade: true,
			})
		)
		.pipe(cssbeautify())
		// .pipe(dest(path.build.css))
		.pipe(
			cssnano({
				zindex: false,
				discardComments: {
					removeAll: true,
				},
			})
		)
		.pipe(removeComments())
		.pipe(rename('custom.min.css'))
		.pipe(dest(path.build.css))
	cb();
}

function cssWatch(cb) {
	return src(path.src.css, {base: srcPath + 'assets/scss/'})
		.pipe(sourcemaps.init())
		.pipe(
			plumber({
				errorHandler: function (err) {
					notify.onError({
						title: 'SCSS Error',
						message: 'Error: <%= error.message %>',
					})(err);
					this.emit('end');
				},
			})
		)
		.pipe(
			sass({
				includePaths: './node_modules/',
			})
		)
		.pipe(sourcemaps.write())
		.pipe(rename('custom.min.css'))
		.pipe(dest(path.build.css))
	cb();
}

function js(cb) {
	return src(path.src.js, {base: srcPath + 'assets/js/'})
		.pipe(
			plumber({
				errorHandler: function (err) {
					notify.onError({
						title: 'JS Error',
						message: 'Error: <%= error.message %>',
					})(err);
					this.emit('end');
				},
			})
		)
		.pipe(
			webpackStream({
				mode: 'production',
				output: {
					filename: 'custom.min.js',
				},
				module: {
					rules: [
						{
							test: /\.(js)$/,
							exclude: /(node_modules)/,
							loader: 'babel-loader',
							query: {
								presets: ['@babel/preset-env'],
							},
						},
					],
				},
			})
		)
		.pipe(dest(path.build.js));
	cb();
}

function jsWatch(cb) {
	return src(path.src.js, {base: srcPath + 'assets/js/'})
		.pipe(
			plumber({
				errorHandler: function (err) {
					notify.onError({
						title: 'JS Error',
						message: 'Error: <%= error.message %>',
					})(err);
					this.emit('end');
				},
			})
		)
		.pipe(
			webpackStream({
				mode: 'development',
				output: {
					filename: 'custom.min.js',
				},
			})
		)
		.pipe(dest(path.build.js));
	cb();
}


function fonts(cb) {
	return src(path.src.fonts)
		.pipe(dest(path.build.fonts));
	cb();
}

function clean(cb) {
	return del(path.clean);

	cb();
}

function watchFiles() {
	gulp.watch([path.watch.css], cssWatch);
	gulp.watch([path.watch.js], jsWatch);
	gulp.watch([path.watch.fonts], fonts);
}

const build = gulp.series(clean, gulp.parallel(css, js, fonts));
const watch = gulp.parallel(css, js, fonts, watchFiles);

/* Exports Tasks */
exports.css = css;
exports.js = js;
exports.fonts = fonts;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;
